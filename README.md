# monitoring

This is the TIG (Telegraf, InfluxDB, Grafana) setup based on Docker for a monitoring cloud VM located at vultr.com.
There are several other hosts who dump their data on the InfluxDB backend, e.g. my Raspberry Pi and another cloud VM responsible for hosting the UniFi software.

It uses Traefik as a reverse-proxy solution with easy SSL certificate generation.